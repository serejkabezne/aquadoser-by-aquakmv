#include <Wire.h>
#define DS3231_ADDRESS 0x68

#define ENCODER_TYPE 2      // тип энкодера (0 или 1). Если энкодер работает некорректно (пропуск шагов), смените тип
#define ENC_REVERSE 0       // 1 - инвертировать энкодер, 0 - нет
#define DRIVER_VERSION 1    // 0 - маркировка драйвера дисплея кончается на 4АТ, 1 - на 4Т


///--------------------------------------------------------------------------------------------ВЕРСИЯ ПРОШИВКИ
#define VER "DOSER ver_02_09_2021"
//#define VER "DOSER by AquaKmv.com"
///--------------------------------------------------------------------------------------------ВЕРСИЯ ПРОШИВКИ

///----названия на главном экране
#define P1 "Ca"
#define P2 "NP"
#define P3 "Fe"
#define P4 "Ми"
///----названия на главном экране

///----названия вменю
#define P1_ "Калий 1"
#define P2_ "Макро 2"
#define P3_ "Железо 3"
#define P4_ "Микро 4"
///----названия вменю

#define PERIOD 59  // максимальный период срабатывания помпы

///----Энкодер
#define CLK 3
#define DT 2
#define SW 0
///----Энкодер

//////----------------Распиновка помп
/*#define P1_Pin 10
  #define P2_Pin 6
  #define P3_Pin 5
  #define P4_Pin 4*/
#define P1_Pin 5
#define P2_Pin 6
#define P3_Pin 10
#define P4_Pin 4
//////----------------Распиновка помп
#define DELAY_MENU 200
#include "GyverEncoder.h"
Encoder enc1(CLK, DT, SW);

#include <EEPROM.h>


#include "LCD_1602_RUS.h"

// -------- АВТОВЫБОР ОПРЕДЕЛЕНИЯ ДИСПЛЕЯ-------------
// Если кончается на 4Т - это 0х27. Если на 4АТ - 0х3f
#if (DRIVER_VERSION)
LCD_1602_RUS lcd(0x27, 20, 4);
#else
LCD_1602_RUS lcd(0x3f, 20, 4);
#endif
// -------- АВТОВЫБОР ОПРЕДЕЛЕНИЯ ДИСПЛЕЯ-------------
byte lcd_off[2]={8,21};
int8_t menu_val = 1;
int8_t menu_val_2 = 0;
//int8_t current_set;
//int8_t menu_set;
int8_t pump_1[10] = {7, 45, 30, 0, 0, 0, 1, 1, 1, 1};
int8_t pump_2[10] = {21, 22, 23};
int8_t pump_3[10] = {13, 32, 33};
int8_t pump_4[10] = {14, 42, 23};
int8_t h, m, s, day, date, month, year;
int8_t pump_test[4] = {0, 0, 0, 0};
int lcd_on = 0;
byte flag = 0;
double counter, lcd_count = 0;


void setup() {
  enc1.setType(TYPE2);
// -------- ЭНКОДЕР-------------
#if (ENC_REVERSE)
enc1.setDirection(NORM);
#else
enc1.setDirection(REVERSE);
#endif
// -------- ЭНКОДЕР-------------

  attachInterrupt(0, isrCLK, CHANGE);    // прерывание на 2 пине! CLK у энка
  attachInterrupt(1, isrDT, CHANGE);    // прерывание на 3 пине! DT у энка

  pinMode(P1_Pin, OUTPUT);
  pinMode(P2_Pin, OUTPUT);
  pinMode(P3_Pin, OUTPUT);
  pinMode(P4_Pin, OUTPUT);
  Wire.begin();
  Wire.begin();
  lcd.init();
  lcd.backlight();
  lcd.clear();

  // --------------------- СБРОС НАСТРОЕК ---------------------
  if (!digitalRead(SW)) {          // если нажат энкодер, сбросить настройки до 1
    
  ////помпа1
  EEPROM.write(10, 7);
  EEPROM.write(12, 10);
  EEPROM.write(14, 5);
  for (int i = 3; i < 10; i++) {
    EEPROM.write(10 + i * 2, 0);
  }
  ////помпа2
  EEPROM.write(30, 8);
  EEPROM.write(32, 30);
  EEPROM.write(34, 10);
  for (int i = 3; i < 10; i++) {
    EEPROM.write(30 + i * 2, 0);
  }
  ////помпа3
  EEPROM.write(50, 9);
  EEPROM.write(52, 45);
  EEPROM.write(54, 15);
  for (int i = 3; i < 10; i++) {
    EEPROM.write(50 + i * 2, 0);
  }
  ////помпа4
  EEPROM.write(70, 10);
  EEPROM.write(72, 55);
  EEPROM.write(74, 20);
  for (int i = 3; i < 10; i++) {
    EEPROM.write(70 + i * 2, 0);
  }


  EEPROM.write(200, 1);


    lcd.setCursor(0, 0);
    lcd.print("Reset settings");
  }
  while (!digitalRead(SW));        // ждём отпускания кнопки
  lcd.clear();
  // очищаем дисплей, продолжаем работу
  ////////// --------------------- СБРОС НАСТРОЕК ---------------------

  //---------------------------память чтение

  ////помпа1
  for (int i = 0; i < 10; i++) {
    pump_1[i] = EEPROM.read(10 + i * 2);
  }
/*pump_1[0] = EEPROM.read(10);
pump_1[1] = EEPROM.read(12);
pump_1[2] = EEPROM.read(14);
pump_1[3] = EEPROM.read(16);
pump_1[4] = EEPROM.read(18);
pump_1[5] = EEPROM.read(20);
pump_1[6] = EEPROM.read(22);
pump_1[7] = EEPROM.read(24);
pump_1[8] = EEPROM.read(26);
pump_1[9] = EEPROM.read(28);
  */
  ////помпа2
  for (int i = 0; i < 10; i++) {
    pump_2[i] = EEPROM.read(30 + i * 2);
  }
  ////помпа3
  for (int i = 0; i < 10; i++) {
    pump_3[i] = EEPROM.read(50 + i * 2);
  }
  ////помпа4
  for (int i = 0; i < 10; i++) {
    pump_4[i] = EEPROM.read(70 + i * 2);
  }


  lcd_on = EEPROM.read(200);

  //---------------------------память чтение
}

// функции с часами =======================================================================================================
byte decToBcd(byte val) {
  // Convert normal decimal numbers to binary coded decimal
  return ( (val / 10 * 16) + (val % 10) );
}

byte bcdToDec(byte val) {
  // Convert binary coded decimal to normal decimal numbers
  return ( (val / 16 * 10) + (val % 16) );
}



void setMinute(byte min1) {

  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x01); //stop Oscillator
  Wire.write(decToBcd(min1));
  Wire.endTransmission();
}

void setHour(byte hour1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x02); //stop Oscillator
  Wire.write(decToBcd(hour1));
  Wire.endTransmission();
}

void setDay(byte day1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x03); //stop Oscillator
  Wire.write(decToBcd(day1));
  Wire.endTransmission();
}

void setDate(byte date1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x04); //stop Oscillator
  Wire.write(decToBcd(date1));
  Wire.endTransmission();
}

void setMonth(byte month1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x05); //stop Oscillator
  Wire.write(decToBcd(month1));
  Wire.endTransmission();
}

void setYear(byte year1) {
  Wire.beginTransmission(DS3231_ADDRESS);
  Wire.write(0x06); //stop Oscillator
  Wire.write(decToBcd(year1));
  Wire.endTransmission();
}

void READ_TIME_RTC() {
  Wire.beginTransmission(DS3231_ADDRESS);        //104 is DS3231 device address
  Wire.write(0x00);                                  //Start at register 0
  Wire.endTransmission();
  Wire.requestFrom(DS3231_ADDRESS, 7);           //Request seven bytes
  if (Wire.available()) {
    s = Wire.read();                           //Get second
    m = Wire.read();                           //Get minute
    h   = Wire.read();                           //Get hour
    day     = Wire.read();
    date    = Wire.read();
    month   = Wire.read();                           //Get month
    year    = Wire.read();

    s = (((s & B11110000) >> 4) * 10 + (s & B00001111)); //Convert BCD to decimal
    m = (((m & B11110000) >> 4) * 10 + (m & B00001111));
    h   = (((h & B00110000) >> 4) * 10 + (h & B00001111));   //Convert BCD to decimal (assume 24 hour mode)
    day     = (day & B00000111); // 1-7
    date    = (((date & B00110000) >> 4) * 10 + (date & B00001111));     //Convert BCD to decimal  1-31
    month   = (((month & B00010000) >> 4) * 10 + (month & B00001111));   //msb7 is century overflow
    year    = (((year & B11110000) >> 4) * 10 + (year & B00001111));
    //  Serial.print(hour23);
  }
}
//
// ========================================================================================================================
// ========================================================================================================================

void isrCLK() {
  enc1.tick();  // отработка в прерывании
}
void isrDT() {
  enc1.tick();  // отработка в прерывании
}

void LCD_Backlight() {

   if (lcd_on == 0) {
     if ((h>lcd_off[0])&&(h<lcd_off[1])) {
       lcd.backlight();
     }
     else {

       if (flag == 1) {
         lcd_count++;
         lcd.backlight();
       }

       if (lcd_count == DELAY_MENU ) {
         lcd_count = 0;
         lcd.noBacklight();
         flag = 0;
       }
     }

    }
  if (lcd_on == 1) {
    if (flag == 1) {
      lcd_count++;
      lcd.backlight();
    }

    if (lcd_count == DELAY_MENU ) {
      lcd_count = 0;
      lcd.noBacklight();
      flag = 0;
    }
  }
  if (lcd_on == 2) {
    lcd.backlight();
  }
}

void EEPROM_WR() {
  ////помпа1
  for (int i = 0; i < 10; i++) {
    EEPROM.write(10 + i * 2, pump_1[i]);
  }
  ////помпа2
  for (int i = 0; i < 10; i++) {
    EEPROM.write(30 + i * 2, pump_2[i]);
  }
  ////помпа3
  for (int i = 0; i < 10; i++) {
    EEPROM.write(50 + i * 2, pump_3[i]);
  }
  ////помпа4
  for (int i = 0; i < 10; i++) {
    EEPROM.write(70 + i * 2, pump_4[i]);
  }


  EEPROM.write(200, lcd_on);
}

void loop() {
  READ_TIME_RTC();
  enc1.tick();
  MAIN();
  PUMPS();
  LCD_Backlight();
}
